#include <condition_variable>
#include <functional>
#include <iostream>
#include <list>
#include <mutex>
#include <thread>
#include <vector>

using namespace std;

class Workers {
  /// Number of workers.
  size_t count;
  /// Worker threads.
  vector<thread> threads;

  mutex mtx; // For simplicity, use only one mutex

  list<function<void()>> tasks;
  condition_variable cv;

  bool stop_threads = false;
  bool stop_threads_on_completed_tasks = false;

  /// Number of timeout tasks not yet added to the tasks list
  size_t timeout_task_count = 0;

public:
  Workers(size_t worker_count) {
    count = worker_count;
  }

  /// Start worker threads
  void start() {
    for (size_t i = 0; i < count; i++) {
      threads.emplace_back([this] {
        while (true) {
          function<void()> task;
          {
            unique_lock<mutex> lock(mtx);
            while (tasks.empty() && !stop_threads)
              cv.wait(lock);
            if (tasks.empty() && stop_threads)
              return;
            task = *tasks.begin();
            tasks.pop_front();
          }
          task();

          unique_lock<mutex> lock(mtx);
          if (stop_threads_on_completed_tasks && tasks.empty() && timeout_task_count == 0) { // If tasks have been completed
            stop_threads = true;
            lock.unlock();
            cv.notify_all(); // Notify all other worker threads to stop
            return;
          }
        }
      });
    }
  }

  /// Add task to be processed by worker(s).
  void post(const function<void()> &task) {
    {
      unique_lock<mutex> lock(mtx);
      tasks.emplace_back(task);
    }
    cv.notify_one();
  }

  /// Add task to be processed by worker(s) after a given delay.
  void post_timeout(const function<void()> &task, int ms) {
    unique_lock<mutex> lock(mtx);
    timeout_task_count++;
    thread timeout_thread([this, task, ms] {
      this_thread::sleep_for(chrono::milliseconds(ms));
      {
        unique_lock<mutex> lock(mtx);
        tasks.emplace_back(task);
        timeout_task_count--;
        cv.notify_one(); // Special case: notify when mtx is locked to be sure cv is still alive (issue detected through thread sanitizer)
      }
    });
    timeout_thread.detach(); // Let thread continue after timeout_thread is destroyed
  }

  /// Stop threads when tasks have been completed.
  void stop() {
    {
      unique_lock<mutex> lock(mtx);
      if (tasks.empty() && timeout_task_count == 0) { // If tasks already have been completed
        stop_threads = true;
        lock.unlock();
        cv.notify_all(); // Notify all worker threads to stop
      } else
        stop_threads_on_completed_tasks = true;
    }
  }

  /// Wait for threads to be stopped.
  void join() {
    for (auto &thread : threads)
      thread.join();
  }
};

int main() {
  Workers worker_threads(4);
  Workers event_loop(1);

  worker_threads.start(); // Create 4 internal threads
  event_loop.start();     // Create 1 internal thread

  worker_threads.post([] {
    cout << "task A" << endl;
  });
  worker_threads.post([] {
    cout << "task B" << endl;
    // Might run in parallel with task A
  });

  event_loop.post([] {
    cout << "task C" << endl;
    // Might run in parallel with task A and B
  });
  event_loop.post([] {
    cout << "task D" << endl;
    // Will run after task C
    // Might run in parallel with task A and B
  });

  event_loop.post_timeout([] {
    cout << "task E" << endl;
  }, 2000); // Run task after 2000ms

  event_loop.post_timeout([] {
    cout << "task F" << endl;
    // Will run before task E
  }, 1000); // Run task after 1000ms

  worker_threads.stop();
  event_loop.stop();

  worker_threads.join(); // Calls join() on the threads
  event_loop.join();     // Calls join() on the thread
}
