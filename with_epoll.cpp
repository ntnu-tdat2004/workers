#include <condition_variable>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <mutex>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <thread>
#include <unistd.h>
#include <vector>

using namespace std;

class Workers {
  /// Number of workers.
  size_t count;
  /// epoll_wait and worker threads.
  vector<thread> threads;

  mutex mtx; // For simplicity, use only one mutex

  list<function<void()>> tasks;
  /// Tasks added by post_timeout, but not yet added to tasks list
  list<function<void()>> postponed_tasks;
  condition_variable cv;

  bool stop_threads = false;
  bool stop_threads_on_completed_tasks = false;

  int epoll_fd;
  /// Map from timeout_fd to postponed_task iterator
  map<int, list<function<void()>>::iterator> fd_to_postponed_task;

public:
  Workers(size_t worker_count) {
    count = worker_count;
    epoll_fd = epoll_create1(0);
  }

  /// Start epoll_wait and worker threads
  void start() {
    threads.emplace_back([this] {
      // For simplicity, no error handling

      vector<epoll_event> events(128);
      while (true) {
        auto event_count = epoll_wait(epoll_fd, events.data(), events.size(), -1);
        {
          unique_lock<mutex> lock(mtx);
          if (stop_threads)
            return;
        }
        for (int i = 0; i < event_count; i++) {
          unique_lock<mutex> lock(mtx);
          auto it = fd_to_postponed_task.find(events[i].data.fd);
          if (it != fd_to_postponed_task.end()) { // If postponed task found
            epoll_ctl(epoll_fd, EPOLL_CTL_DEL, events[i].data.fd, nullptr);
            close(it->first);                // Close timeout_fd (it->first)
            tasks.emplace_back(*it->second); // *it->second corresponds to the postponed task
            postponed_tasks.erase(it->second);
            fd_to_postponed_task.erase(it);
            cv.notify_one();
          }
        }
      }
    });

    for (size_t i = 0; i < count; i++) {
      threads.emplace_back([this] {
        while (true) {
          function<void()> task;
          {
            unique_lock<mutex> lock(mtx);
            while (tasks.empty() && !stop_threads)
              cv.wait(lock);
            if (tasks.empty() && stop_threads)
              return;
            task = *tasks.begin();
            tasks.pop_front();
          }
          task();

          unique_lock<mutex> lock(mtx);
          if (stop_threads_on_completed_tasks && tasks.empty() && postponed_tasks.empty()) { // If tasks have been completed
            stop_threads = true;
            lock.unlock();
            cv.notify_all();        // Notify all other worker threads to stop
            post_timeout([] {}, 1); // Notify epoll thread to stop as well
            return;
          }
        }
      });
    }
  }

  /// Add task to be processed by worker(s).
  void post(const function<void()> &task) {
    {
      unique_lock<mutex> lock(mtx);
      tasks.emplace_back(task);
    }
    cv.notify_one();
  }

  /// Add task to be processed by worker(s) after a given delay.
  void post_timeout(const function<void()> &task, int ms) {
    // For simplicity, no error handling

    epoll_event timeout;
    timeout.events = EPOLLIN;
    int fd = timerfd_create(CLOCK_MONOTONIC, 0);
    timeout.data.fd = fd;
    {
      unique_lock<mutex> lock(mtx);
      postponed_tasks.emplace_back(task);
      fd_to_postponed_task.emplace(fd, prev(postponed_tasks.end()));
    }
    itimerspec ts;
    ts.it_value.tv_sec = ms / 1000;              // Delay before initial event
    ts.it_value.tv_nsec = (ms % 1000) * 1000000; // Delay before initial event
    ts.it_interval.tv_sec = 0;                   // Period between repeated events after initial delay (0: disabled)
    ts.it_interval.tv_nsec = 0;                  // Period between repeated events after initial delay (0: disabled)
    timerfd_settime(timeout.data.fd, 0, &ts, nullptr);
    // Add timeout to epoll so that it is monitored by epoll_wait:
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, timeout.data.fd, &timeout);
  }

  /// Stop threads when tasks have been completed.
  void stop() {
    {
      unique_lock<mutex> lock(mtx);
      if (tasks.empty() && postponed_tasks.empty()) { // If tasks already have been completed
        stop_threads = true;
        lock.unlock();
        cv.notify_all();        // Notify all worker threads to stop
        post_timeout([] {}, 1); // Notify epoll thread to stop as well
      } else
        stop_threads_on_completed_tasks = true;
    }
  }

  /// Wait for threads to be stopped.
  void join() {
    for (auto &thread : threads)
      thread.join();
  }
};

int main() {
  Workers worker_threads(4);
  Workers event_loop(1);

  worker_threads.start(); // Create 4 internal threads
  event_loop.start();     // Create 1 internal thread

  worker_threads.post([] {
    cout << "task A" << endl;
  });
  worker_threads.post([] {
    cout << "task B" << endl;
    // Might run in parallel with task A
  });

  event_loop.post([] {
    cout << "task C" << endl;
    // Might run in parallel with task A and B
  });
  event_loop.post([] {
    cout << "task D" << endl;
    // Will run after task C
    // Might run in parallel with task A and B
  });

  event_loop.post_timeout([] {
    cout << "task E" << endl;
  }, 2000); // Run task after 2000ms

  event_loop.post_timeout([] {
    cout << "task F" << endl;
    // Will run before task E
  }, 1000); // Run task after 1000ms

  worker_threads.stop();
  event_loop.stop();

  worker_threads.join(); // Calls join() on the threads
  event_loop.join();     // Calls join() on the thread
}
